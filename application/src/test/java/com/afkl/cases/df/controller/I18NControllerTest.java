package com.afkl.cases.df.controller;

import com.afkl.cases.df.AbstractTestSuite;
import com.afkl.cases.df.exception.FareNotFoundException;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.service.TravelService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class I18NControllerTest extends AbstractTestSuite {
  @Test
  public void itShouldReturnDictionary() throws Exception {
    mockMvc.perform(get("/api/i18n"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.search.header._nl").value("Zoek een vlucht"));
  }
}
