package com.afkl.cases.df.controller;

import com.afkl.cases.df.AbstractTestSuite;
import com.afkl.cases.df.exception.FareNotFoundException;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.service.TravelService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TravelControllerTest extends AbstractTestSuite {
  @MockBean
  private TravelService travelService;

  @Before
  public void setup() {
    List<Airport> airports = new ArrayList<>();
    airports.add(new Airport("AMS", "Schiphol", "Schiphol Test", null, emptySet()));

    Fare fare = new Fare(10.99, Currency.EUR, "AMS", "BRU");

    // Happy Case Setup
    when(travelService.searchAirports("ams", AirportLangs.NL, 5))
        .thenReturn(new Resources<>(airports));
    when(travelService.calculateFare("AMS", "BRU", Currency.EUR))
        .thenReturn(new Resource<>(fare));

    // Error Case Setup
    when(travelService.searchAirports("wrong", AirportLangs.NL, 5))
        .thenReturn(new Resources<>(emptyList()));
    when(travelService.calculateFare("AMS", "WRONG", Currency.EUR))
        .thenThrow(new FareNotFoundException());
  }

  @Test
  public void itShouldReturnAirportsOnSearch() throws Exception {
    mockMvc.perform(get("/api/airports").param("q", "ams"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.airportList").isArray())
        .andExpect(jsonPath("$._embedded.airportList[0]").exists())
        .andExpect(jsonPath("$._embedded.airportList[0].code").value("AMS"))
        .andExpect(jsonPath("$._embedded.airportList[0].name").value("Schiphol"))
        .andExpect(jsonPath("$._embedded.airportList[0].description").value("Schiphol Test"));
  }

  @Test
  public void itShouldReturnEmptyOnInvalidSearch() throws Exception {
    mockMvc.perform(get("/api/airports").param("q", "wrong"))
        .andExpect(status().isOk());
  }

  @Test
  public void itShouldReturnFareForTwoValidAirportCodes() throws Exception {
    mockMvc.perform(get("/api/fares/AMS/BRU"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.origin").value("AMS"))
        .andExpect(jsonPath("$.destination").value("BRU"))
        .andExpect(jsonPath("$.currency").value("EUR"))
        .andExpect(jsonPath("$.amount").value(10.99));
  }

  @Test
  public void itShouldReturnNotFoundForInvalidAirportCodes() throws Exception {
    mockMvc.perform(get("/api/fares/AMS/WRONG"))
        .andExpect(status().isNotFound());
  }
}
