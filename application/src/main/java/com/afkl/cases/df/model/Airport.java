package com.afkl.cases.df.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Airport {
  private String code, name, description;
  private Airport parent;
  private Set<Airport> children;
}
