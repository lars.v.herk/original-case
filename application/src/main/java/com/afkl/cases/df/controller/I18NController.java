package com.afkl.cases.df.controller;

import lombok.extern.log4j.Log4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


@Log4j
@RestController
@RequestMapping("/api/i18n")
public class I18NController {
  @GetMapping(produces = "application/json")
  public ResponseEntity getI18NDictionary() {
    try {
      ClassPathResource resource = new ClassPathResource("i18n/dict.json");
      byte[] bytes = FileCopyUtils.copyToByteArray(resource.getInputStream());

      return ResponseEntity.ok(new String(bytes, StandardCharsets.UTF_8));
    } catch(IOException ex) {
      log.error("I18N Dictionary parse failed:", ex);
      return ResponseEntity
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .build();
    }
  }
}
