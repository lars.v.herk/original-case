package com.afkl.cases.df.config.feign;

import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

public class TravelClientConfig {

  @Value("${application.api.travel.oauth.token_endpoint}")
  private String tokenEndpoint;

  @Value("${application.api.travel.oauth.client_id}")
  private String clientId;

  @Value("${application.api.travel.oauth.client_secret}")
  private String clientSecret;

  @Bean
  public RequestInterceptor requestInterceptor() {
    return new OAuth2FeignRequestInterceptor(new DefaultOAuth2ClientContext(), resource());
  }

  private OAuth2ProtectedResourceDetails resource() {
    final ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
    details.setAccessTokenUri(this.tokenEndpoint);
    details.setClientId(this.clientId);
    details.setClientSecret(this.clientSecret);
    return details;
  }
}
