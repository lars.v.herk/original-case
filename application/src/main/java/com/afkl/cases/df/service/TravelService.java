package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;

public interface TravelService {
  Resources<Airport> searchAirports(String query, AirportLangs lang, int pageSize);
  Resource<Fare> calculateFare(String origin, String destination, Currency currency);
}
