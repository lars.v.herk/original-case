package com.afkl.cases.df.client.travel;

import com.afkl.cases.df.client.travel.fallback.TravelClientFallbackFactory;
import com.afkl.cases.df.config.feign.TravelClientConfig;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
    name = "travel",
    url = "${application.api.travel.url}",
    fallbackFactory = TravelClientFallbackFactory.class,
    configuration = TravelClientConfig.class
)
public interface TravelClient {
  @RequestMapping(method = RequestMethod.GET, value = "/airports")
  Resources<Airport> searchAirports(@RequestParam("term") String query,
                            @RequestParam("lang") AirportLangs lang,
                            @RequestParam("size") int pageSize);

  @RequestMapping(method = RequestMethod.GET, value = "/fares/{origin}/{destination}")
  Resource<Fare> calculateFare(@PathVariable("origin") String origin,
                               @PathVariable("destination") String destination,
                               @RequestParam("currency") Currency currency);
}
