package com.afkl.cases.df.client.travel.fallback;

import com.afkl.cases.df.client.travel.TravelClient;
import com.afkl.cases.df.exception.FareNotFoundException;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;

import java.util.ArrayList;

@Log4j
@AllArgsConstructor
public class TravelClientFallback implements TravelClient {
  private final Throwable cause;

  @Override
  public Resources<Airport> searchAirports(String query, AirportLangs lang, int pageSize) {
    if (cause != null) {
      log.warn("Travel API connect failed, falling back:", cause);
    }

    return new Resources<>(new ArrayList<>());
  }

  @Override
  public Resource<Fare> calculateFare(String origin, String destination, Currency currency) {
    if (cause != null) {
      log.warn("Travel API connect failed, falling back:", cause);
    }

    return null;
  }
}
