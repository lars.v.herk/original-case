package com.afkl.cases.df.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"com.afkl.cases.df.client"})
public class FeignConfig {
}
