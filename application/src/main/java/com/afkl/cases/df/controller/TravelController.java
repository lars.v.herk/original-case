package com.afkl.cases.df.controller;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.service.TravelService;
import lombok.AllArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class TravelController {
  private TravelService service;

  @GetMapping("airports")
  public ResponseEntity<Resources<Airport>> searchAirports(
      @RequestParam("q") @NotBlank String query,
      @RequestParam(value = "lang", defaultValue = "NL") AirportLangs lang,
      @RequestParam(value = "limit", defaultValue = "5") int limit) {
    return ResponseEntity.ok(service.searchAirports(query, lang, limit));
  }

  @GetMapping("fares/{origin}/{destination}")
  public ResponseEntity<Resource<Fare>> calculateFare(
      @PathVariable("origin") String origin,
      @PathVariable("destination") String destination,
      @RequestParam(value = "currency", defaultValue = "EUR") Currency currency) {
    return ResponseEntity.ok(service.calculateFare(origin, destination, currency));
  }
}
