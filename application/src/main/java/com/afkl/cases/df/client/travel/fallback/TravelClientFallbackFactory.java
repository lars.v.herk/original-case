package com.afkl.cases.df.client.travel.fallback;

import com.afkl.cases.df.client.travel.TravelClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class TravelClientFallbackFactory implements FallbackFactory<TravelClient> {

  @Override
  public TravelClient create(Throwable throwable) {
    return new TravelClientFallback(throwable);
  }
}
