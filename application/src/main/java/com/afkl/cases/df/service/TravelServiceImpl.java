package com.afkl.cases.df.service;

import com.afkl.cases.df.exception.FareNotFoundException;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.client.travel.TravelClient;
import com.afkl.cases.df.model.AirportLangs;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import lombok.AllArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Service;

@Service("airportService")
@AllArgsConstructor
public class TravelServiceImpl implements TravelService {
  private TravelClient travel;

  @Override
  public Resources<Airport> searchAirports(@NotEmpty String query, AirportLangs lang, int pageSize) {
    return travel.searchAirports(query, lang, pageSize);
  }

  @Override
  public Resource<Fare> calculateFare(String origin, String destination, Currency currency) {
    Resource<Fare> fare = travel.calculateFare(origin, destination, currency);
    if (fare == null) throw new FareNotFoundException();
    return fare;
  }
}
