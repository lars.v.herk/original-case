package com.afkl.cases.df.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Fare {
    private double amount;
    private Currency currency;
    private String origin, destination;
}
