Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/index.html](http://localhost:9000/travel/index.html)

## Completion remarks & Notes

In order to run the full intended setup, you will need to run `./gradlew clean build` once to generate the necessary artifacts for Docker.

Once completed, you can run `docker-compose build` & `docker-compose up -d` to start all the services.

---

The Case has been implemented using Angular 6 for the frontend, and the required Spring Boot backend.  
It can still be run using the standard `./gradlew bootRun` command, but it is advised to run `./gradlew clean build` once before continuing.

For metrics collection and tracking, I have used Prometheus & Grafana. These are automatically provisioned using Docker containers.  
To build all the Docker containers I had to manually include a built artifact of the Travel API Mock, since using a dedicated registry, or publishing
on the Docker Hub wasn't immediately possible.

After running `docker-compose up -d`, the application will be available [on port 9000, under `/travel`.](http://localhost:9000/travel)

The login for Grafana is still the default `admin:admin` setup, and can be viewed on [on port 3000 in your browser.](http://localhost:3000)  
The Grafana monitoring dashboard can be found under the name 'Travel Client Monitor'. [Direct Link](http://localhost:3000/d/q-7htWAik/travel-client-monitor)
