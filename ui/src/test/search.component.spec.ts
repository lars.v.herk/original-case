import { async, TestBed } from '@angular/core/testing';

import { empty, of } from 'rxjs';

import { AppSandbox } from 'app/app.sandbox';
import { SearchComponent } from 'app/components/container/search/search.component';
import { I18NPipe } from 'app/shared/i18n/pipe/i18n.pipe';
import { I18NService } from 'app/shared/i18n/service/i18n.service';
import { ReactiveFormsModule } from '@angular/forms';

describe('SearchComponent', () => {
  let fixture;
  let component;

  let mockI18N: Partial<I18NService>;
  let mockSandbox: Partial<AppSandbox>;

  beforeEach(async(() => {
    mockI18N = {
      translate: () => of('I18NMOCK')
    };

    mockSandbox = {
      query$: of({}),
      updateQuery: () => {},
      searchAirports: () => empty()
    };

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        I18NPipe,
        SearchComponent
      ],
      providers: [
        { provide: I18NService, useValue: mockI18N },
        { provide: AppSandbox, useValue: mockSandbox }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should update the query object if origin input changes', async(() => {
    const sb = TestBed.get(AppSandbox);
    spyOn(sb, 'searchAirports').and.returnValue(of([]));

    component.form.patchValue({ origin: 'AMS' });

    // Take input debounce into account
    setTimeout(() => {
      expect(sb.searchAirports).toHaveBeenCalledWith('AMS');
    }, 500);
  }));

  it('should update the query object if destination input changes', async(() => {
    const sb = TestBed.get(AppSandbox);
    spyOn(sb, 'searchAirports').and.returnValue(of([]));

    component.form.patchValue({ destination: 'AMS' });

    // Take input debounce into account
    setTimeout(() => {
      expect(sb.searchAirports).toHaveBeenCalledWith('AMS');
    }, 500);
  }));
});
