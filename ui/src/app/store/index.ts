import { ActionReducerMap, combineReducers } from '@ngrx/store';

import { I18NConfig } from 'app/shared/model/i18n-config.model';
import { i18nReducer } from 'app/store/i18n';

import { Fare } from 'app/shared/model/fare.model';
import { fareReducer } from 'app/store/fare';

import { Query } from 'app/shared/model/query.model';
import { queryReducer } from 'app/store/query';

export interface AppState {
  readonly i18n: I18NConfig;
  readonly query: Query;
  readonly fare: Fare;
}

export const reducers: ActionReducerMap<AppState> = {
  i18n: i18nReducer,
  query: queryReducer,
  fare: fareReducer
};

export function reducerFactory() {
  return combineReducers(reducers);
}
