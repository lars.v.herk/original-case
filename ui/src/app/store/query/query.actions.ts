import { Action } from '@ngrx/store';

import { Query } from 'app/shared/model/query.model';

export enum QueryActionTypes {
  UPDATE_QUERY = '[Query] Update Query'
}

export class UpdateQuery implements Action {
  readonly type = QueryActionTypes.UPDATE_QUERY;

  constructor(readonly query: Query) { }
}

export type QueryActions
  = UpdateQuery;
