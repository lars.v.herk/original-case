import { Query } from 'app/shared/model/query.model';

import { QueryActions, QueryActionTypes } from 'app/store/query/query.actions';

const initialState = (): Query => ({});

export function queryReducer(state: Query = initialState(), action: QueryActions): Query {
  switch (action.type) {
    case QueryActionTypes.UPDATE_QUERY:
      return Object.assign({}, state, action.query);
  }
  return state;
}
