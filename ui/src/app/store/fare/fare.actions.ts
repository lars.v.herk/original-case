import { Action } from '@ngrx/store';
import { Fare } from 'app/shared/model/fare.model';

export enum FareActionTypes {
  SET_FARE = '[Fare] Set Fare',
  CLEAR_FARE = '[Fare] Clear Fare'
}

export class SetFare implements Action {
  readonly type = FareActionTypes.SET_FARE;

  constructor(readonly fare: Fare) { }
}

export class ClearFare implements Action {
  readonly type = FareActionTypes.CLEAR_FARE;
}

export type FareActions
  = SetFare
  | ClearFare;
