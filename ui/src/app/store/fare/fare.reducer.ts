import { Fare } from 'app/shared/model/fare.model';
import { FareActions, FareActionTypes } from 'app/store/fare/fare.actions';

const initialState = (): Fare => ({
  amount: -1,
  currency: 'EUR',
  origin: '',
  destination: ''
});

export function fareReducer(state: Fare = initialState(), action: FareActions): Fare {
  switch (action.type) {
    case FareActionTypes.SET_FARE:
      return action.fare;

    case FareActionTypes.CLEAR_FARE:
      return initialState();
  }
  return state;
}
