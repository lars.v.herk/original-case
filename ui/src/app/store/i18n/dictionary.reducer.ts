import { I18NActions, I18NActionTypes } from 'app/store/i18n/i18n.actions';

export const initialState = (): { [key: string]: string } => ({});

export function dictionaryReducer(state: { [key: string]: string } = initialState(), action: I18NActions): { [key: string]: string } {
  switch (action.type) {
    case I18NActionTypes.SET_DICTIONARY:
      return action.dictionary;
  }
  return state;
}
