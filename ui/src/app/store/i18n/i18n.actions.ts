import { Action } from '@ngrx/store';

export enum I18NActionTypes {
  SET_LANGUAGE = '[I18N] Set Language',
  SET_DICTIONARY = '[I18N] Set Dictionary'
}

export class SetLanguage implements Action {
  readonly type = I18NActionTypes.SET_LANGUAGE;

  constructor(readonly lang: 'nl' | 'en') { }
}

export class SetDictionary implements Action {
  readonly type = I18NActionTypes.SET_DICTIONARY;

  constructor(readonly dictionary: { [key: string]: string }) { }
}

export type I18NActions
  = SetLanguage
  | SetDictionary;
