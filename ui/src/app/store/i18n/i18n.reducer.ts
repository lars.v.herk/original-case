import { I18NConfig } from 'app/shared/model/i18n-config.model';
import { combineReducers } from '@ngrx/store';

import { initialState as initLanguage, languageReducer } from 'app/store/i18n/language.reducer';
import { initialState as initDictionary, dictionaryReducer } from 'app/store/i18n/dictionary.reducer';

const initialState = (): I18NConfig => ({
  language: initLanguage(),
  dictionary: initDictionary()
});

export const i18nReducer = combineReducers({
  language: languageReducer,
  dictionary: dictionaryReducer
});
