import { environment } from 'environment';
import { I18NActions, I18NActionTypes } from 'app/store/i18n/i18n.actions';

export const initialState = () => <'nl' | 'en'>environment.I18N_DEFAULT;

export function languageReducer(state: 'nl' | 'en' = initialState(), action: I18NActions): 'nl' | 'en' {
  switch (action.type) {
    case I18NActionTypes.SET_LANGUAGE:
      return action.lang;
  }
  return state;
}
