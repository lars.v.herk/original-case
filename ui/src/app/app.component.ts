import { Component } from '@angular/core';
import { AppSandbox } from 'app/app.sandbox';

@Component({
  selector: 'app-root',
  template: `
    <header>
      <section class="search-flights columns">
        <div class="search-flights__form column is-6-desktop is-offset-3-desktop card">
          <div class="card-content">
            <div class="select is-pulled-right">
              <select [value]="lang$ | async"
                      (change)="handleChangeLang($event)">
                <option value="nl">NL</option>
                <option value="en">EN</option>
              </select>
            </div>
            <app-search-flights>
            </app-search-flights>
          </div>
        </div>
      </section>
    </header>

    <main>
      <div class="container">
        <section class="fare-offers columns">
          <ng-container *ngIf="fare$ | async as fare">

            <app-fare
              *ngIf="fare.amount >= 0; else noFare"
              [query]="query$ | async"
              [fare]="fare"
              class="column is-10-desktop is-offset-1-desktop">
            </app-fare>

            <ng-template #noFare>
              <small *ngIf="!(isLoadingFare$ | async); else loadingFare"
                class="search-flights__results--empty column is-full text is-light has-text-centered">
                {{'search.results.empty' | i18n | async}}

                <span class="icon is-large">
                  <i class="material-icons">airplanemode_inactive</i>
                </span>
              </small>

              <ng-template #loadingFare>
                <small
                  class="search-flights__results--loading column is-full text is-light has-text-centered">
                  {{'search.results.loading' | i18n | async}}

                  <span class="icon is-large">
                    <i class="material-icons">airplanemode_active</i>
                  </span>
                </small>
              </ng-template>
            </ng-template>
          </ng-container>
        </section>
      </div>
    </main>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly lang$ = this.sb.currentLang$;
  readonly query$ = this.sb.query$;
  readonly fare$ = this.sb.fare$;
  readonly isLoadingFare$ = this.sb.isLoadingFare$;

  constructor(private sb: AppSandbox) {}

  handleChangeLang(event) {
    if (['nl', 'en'].indexOf(event.target.value) !== -1) {
      this.sb.setCurrentLang(event.target.value);
    }
  }
}
