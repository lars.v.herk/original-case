import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'environment';

import forEach from 'lodash/forEach';
import get from 'lodash/get';

import { select, Store } from '@ngrx/store';
import { AppState } from 'app/store';
import { SetDictionary, SetLanguage } from 'app/store/i18n';

import { combineLatest, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class I18NService {
  readonly currentLang$ = this.store.pipe(select(state => state.i18n.language));
  readonly dict$ = this.store.pipe(select(state => state.i18n.dictionary));

  private _fallback: boolean;
  private readonly PLACEHOLDER = '%';

  public readonly supportedLangs = [
    { display: 'Nederlands', value: 'nl' },
    { display: 'English', value: 'en' }
  ];

  constructor(private http: HttpClient,
              private store: Store<AppState>) {
  }

  public populateDictionary(): Promise<boolean> {
    this.decideLanguage();
    return new Promise((resolve: Function) => {
      this.http.get('/api/i18n')
        .subscribe((data: { [key: string]: string }) => {
          this.store.dispatch(new SetDictionary(data));
          resolve(true);
        }, () => {
          resolve(false);
        });
    });
  }

  public enableFallback(enable: boolean) {
    this._fallback = enable;
  }

  public use(lang: 'nl' | 'en'): void {
    localStorage.setItem('tr_app_lang', lang);
    this.store.dispatch(new SetLanguage(lang));
  }

  public translate(key: string, words?: string | string[]): Observable<string> {
    return combineLatest(this.currentLang$, this.dict$).pipe(
      map((i18n) => {
        const lang = i18n[0];
        const dict = i18n[1];
        const dictEntry = get(dict, key);

        const defaultTranslation = get(dictEntry, `_${environment.I18N_DEFAULT}`);
        return get(dictEntry, `_${lang}`, this._fallback ? `_${defaultTranslation}` : key);
      }),
      map(translation => {
        return words ? this.replace(translation, words) : translation;
      }),
      catchError(() => of(key))
  );
  }

  public replace(word: string = '', words: string | string[] = '') {
    let translation: string = word;

    const values: string[] = [].concat(words);
    forEach(values, (e, i) => {
      translation = translation.replace(this.PLACEHOLDER.concat(<any>i), e);
    });

    return translation;
  }

  public decideLanguage() {
    if (!localStorage.getItem('tr_app_lang')) {
      let browserLang = window.navigator.language;
      browserLang = browserLang.substr(0, 2).toLowerCase();
      if (browserLang !== 'nl' && browserLang !== 'en') {
        browserLang = 'nl';
      }
      localStorage.setItem('tr_app_lang', browserLang);
    }
    this.use(<'nl' | 'en'>localStorage.getItem('tr_app_lang'));
  }
}

export function initInternationalisation(svc: I18NService): Function {
  return () => svc.populateDictionary();
}
