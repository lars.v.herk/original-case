import { Pipe, PipeTransform } from '@angular/core';
import { I18NService } from '../service/i18n.service';

@Pipe({
  name: 'i18n',
  pure: false
})
export class I18NPipe implements PipeTransform {
  constructor(private i18n: I18NService) {
  }

  transform(value: string, args: string | string[]): any {
    return value ? this.i18n.translate(value, args) : undefined;
  }
}
