export interface Fare {
  readonly amount: number;
  readonly currency: 'EUR' | 'USD';
  readonly origin: string;
  readonly destination: string;
}
