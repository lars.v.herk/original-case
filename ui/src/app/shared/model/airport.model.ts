export interface Airport {
  code: string;
  name: string;
  description: string;
  parent: Airport;
  children: Airport[];
}
