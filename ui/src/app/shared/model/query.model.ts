import { Airport } from 'app/shared/model/airport.model';

export interface Query {
  readonly origin?: Airport;
  readonly destination?: Airport;
}
