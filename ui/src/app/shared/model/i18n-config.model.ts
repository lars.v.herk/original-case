export interface I18NConfig {
  readonly language: 'nl' | 'en';
  readonly dictionary: { [key: string]: string };
}
