import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import get from 'lodash/get';

import { Airport } from 'app/shared/model/airport.model';
import { Fare } from 'app/shared/model/fare.model';

@Injectable({
  providedIn: 'root'
})
export class TravelService {
  constructor(private http: HttpClient) {}

  public searchAirports(term: string, lang: 'NL' | 'EN'): Observable<Airport[]> {
    const opts = {
      params: new HttpParams()
        .set('q', term)
        .set('lang', lang)
    };
    return this.http.get('/api/airports', opts).pipe(
      map((res: any) => get(res, '_embedded.airportList', []))
    );
  }

  public calculateFare(origin: string, destination: string, lang: 'nl' | 'en' = 'nl'): Observable<Fare> {
    const opts = {
      params: new HttpParams()
        .set('currency', lang === 'nl' ? 'EUR' : 'USD')
    };
    const encodedURI = encodeURI(`/api/fares/${origin}/${destination}`);
    return this.http.get(encodedURI, opts).pipe(
      map((fare) => <Fare>fare)
    );
  }
}
