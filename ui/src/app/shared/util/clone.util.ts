import cloneDeep from 'lodash/cloneDeep';

export default function clone<T>(obj: T, forceDeepClone: boolean = false): T {
  try {
    if (forceDeepClone) {
      console.warn('[util/clone] Forcibly switching to helper...');
      return cloneDeep(obj);
    } else {
      return JSON.parse(JSON.stringify(obj));
    }
  } catch (ex) {
    console.warn('[util/clone] Default clone failed, falling back to helper!');
    return cloneDeep(obj);
  }
}
