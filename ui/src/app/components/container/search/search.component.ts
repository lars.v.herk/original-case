import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';

import { Subscription, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, catchError } from 'rxjs/operators';

import { AppSandbox } from 'app/app.sandbox';
import { Airport } from 'app/shared/model/airport.model';
import { Query } from 'app/shared/model/query.model';

@Component({
  selector: 'app-search-flights',
  template: `
    <form class="search-form" [formGroup]="form" novalidate>
      <h1 class="title">{{'search.header' | i18n | async}}</h1>
      <div class="columns">
        <div class="search-form__input field column is-half is-full-mobile">
          <label class="label is-medium">{{'search.form.origin' | i18n | async}}</label>
          <div class="control has-icons-left has-icons-right">
            <input class="input is-medium" type="email" formControlName="origin"
                   [placeholder]="'search.form.origin.placeholder' | i18n | async">
            <span class="icon is-small is-left">
              <i class="material-icons">flight_takeoff</i>
            </span>

            <ul *ngIf="originResults && originResults.length > 0"
                class="search-form__autocomplete-result">
              <li *ngFor="let result of originResults" [title]="result.description"
                (click)="updateQuery({ origin: result })">
                {{result.description}}
              </li>
            </ul>
          </div>
        </div>

        <div class="search-form__input field column is-half is-full-mobile">
          <label class="label is-medium">{{'search.form.destination' | i18n | async}}</label>
          <div class="control has-icons-left has-icons-right">
            <input class="input is-medium" type="email" formControlName="destination"
                   [placeholder]="'search.form.destination.placeholder' | i18n | async">
            <span class="icon is-small is-left">
              <i class="material-icons">flight_land</i>
            </span>

            <ul *ngIf="destinationResults && destinationResults.length > 0"
                class="search-form__autocomplete-result">
              <li *ngFor="let result of destinationResults" [title]="result.description"
                (click)="updateQuery({ destination: result })">
                {{result.description}}
              </li>
            </ul>
          </div>
        </div>
      </div>
    </form>
  `,
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements AfterViewInit, OnDestroy {
  private _subs: Subscription[] = [];

  public form: FormGroup;

  public originResults: Airport[] = [];
  public destinationResults: Airport[] = [];

  constructor(private sb: AppSandbox, fb: FormBuilder) {
    this.form = fb.group({
      origin: fb.control(''),
      destination: fb.control('')
    });

    this._subs.push(
      sb.query$.subscribe(({ origin = <Airport>{}, destination = <Airport>{}}) => {
        this.form.patchValue({
          origin: origin.description || '',
          destination: destination.description || ''
        });
      })
    );
  }

  ngAfterViewInit() {
    this.registerSearchHandlers(
      { input: this.form.get('origin'), handler: (val: Airport[]) => {
        this.originResults = val;
      }},
      { input: this.form.get('destination'), handler: (val: Airport[]) => {
        this.destinationResults = val;
      }}
    );
  }

  ngOnDestroy() {
    this._subs.forEach(sub => sub.unsubscribe());
  }

  public updateQuery(query: Query) {
    this.sb.updateQuery(query);
  }

  private registerSearchHandlers(...args: { input: AbstractControl, handler: (value: Airport[]) => void }[]) {
    this._subs.push(
      ...args.map((arg) => {
        return arg.input.valueChanges.pipe(
          debounceTime(500),
          distinctUntilChanged(),
          switchMap((val) => {
            return !!val
              ? this.sb.searchAirports(val).pipe(
                  catchError(() => of([]))
                )
              : of([]);
          })
        ).subscribe(arg.handler);
      })
    );
  }
}
