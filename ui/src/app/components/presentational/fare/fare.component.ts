import { Component, Input } from '@angular/core';

import { Fare } from 'app/shared/model/fare.model';
import { Query } from 'app/shared/model/query.model';

@Component({
  selector: 'app-fare',
  template: `
    <div class="card fare">
      <div class="card-content fare__content">
        <div class="fare__airline">
          <img src="./assets/img/klm.png" alt="Logo KLM">
        </div>

        <div class="fare__details">
          <div class="fare__destinations">
            <div class="fare__destination">
              <p class="title is-4 has-text-centered">
                <span class="icon is-left">
                  <i class="material-icons">flight_takeoff</i>
                </span>
                {{query.origin.name}}
              </p>
            </div>

            <div class="fare__destination">
              <p class="title is-4 has-text-centered">
                <span class="icon is-left">
                  <i class="material-icons">flight_land</i>
                </span>
                {{query.destination.name}}
              </p>
            </div>
          </div>

          <div class="tag is-info is-large">
            {{fare.amount + ' ' + fare.currency}}
          </div>
        </div>
      </div>
    </div>
  `, styleUrls: ['./fare.component.scss']
})
export class FareComponent {
  @Input() readonly fare: Fare;
  @Input() readonly query: Query;
}
