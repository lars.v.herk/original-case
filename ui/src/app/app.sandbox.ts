import { select, Store } from '@ngrx/store';
import { Injectable, OnDestroy } from '@angular/core';

import { Observable, Subscription, empty, BehaviorSubject, combineLatest } from 'rxjs';
import { flatMap, map, filter, catchError } from 'rxjs/operators';

import { AppState } from 'app/store';
import { I18NService } from 'app/shared/i18n/service/i18n.service';

import { Query } from 'app/shared/model/query.model';
import { UpdateQuery } from 'app/store/query';

import { TravelService } from 'app/shared/service/travel.service';
import { Airport } from 'app/shared/model/airport.model';

import { Fare } from 'app/shared/model/fare.model';
import { SetFare, ClearFare } from 'app/store/fare';

@Injectable({
  providedIn: 'root'
})
export class AppSandbox implements OnDestroy {
  private _subs: Subscription[] = [];
  private _fareLoading = new BehaviorSubject<boolean>(false);

  readonly currentLang$ = this.store.pipe(select(state => state.i18n.language));
  readonly query$ = this.store.pipe(select(state => state.query));
  readonly fare$ = this.store.pipe(select(state => state.fare));
  readonly isLoadingFare$ = this._fareLoading.asObservable();

  constructor(
    private store: Store<AppState>,
    private i18nService: I18NService,
    private travelService: TravelService
  ) {
    this._subs.push(
      combineLatest(this.currentLang$, this.query$).pipe(
        flatMap((params) => {
          const currentLang = params[0];
          const { origin = <Airport>{}, destination = <Airport>{} } = params[1];

          if (origin.code && destination.code) {
            this._fareLoading.next(true);
            this.store.dispatch(new ClearFare());

            return this.travelService
              .calculateFare(origin.code, destination.code, currentLang).pipe(
                catchError(() => {
                  this._fareLoading.next(false);
                  return empty();
                })
              );
          }
          return empty();
        })
      ).subscribe((fare: Fare) => {
        this._fareLoading.next(false);
        this.store.dispatch(new SetFare(fare));
      })
    );
  }

  ngOnDestroy() {
    this._subs.forEach(sub => sub.unsubscribe());
  }

  public setCurrentLang(lang: 'nl' | 'en') {
    this.i18nService.use(lang);
  }

  public searchAirports(term: string): Observable<Airport[]> {
    return this.currentLang$.pipe(
      map(lang => <'NL' | 'EN'>lang.toUpperCase()),
      flatMap(lang => this.travelService.searchAirports(term, lang))
    );
  }

  public updateQuery(query: Query) {
    this.store.dispatch(new UpdateQuery(query));
  }
}
