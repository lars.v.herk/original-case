import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import locale from '@angular/common/locales/nl';

import { environment } from 'environment';

import { StoreModule } from '@ngrx/store';
import { reducerFactory } from './store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { APIBaseInterceptor } from 'app/shared/interceptor/api-base.interceptor';

import { I18NService, initInternationalisation } from 'app/shared/i18n/service/i18n.service';
import { I18NPipe } from 'app/shared/i18n/pipe/i18n.pipe';

import { AppComponent } from './app.component';
import { SearchComponent } from './components/container/search/search.component';
import { FareComponent } from './components/presentational/fare/fare.component';

registerLocaleData(locale, 'nl');

@NgModule({
  declarations: [
    I18NPipe,
    AppComponent,
    SearchComponent,
    FareComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot(undefined, { reducerFactory }),
    !environment.production
      ? StoreDevtoolsModule.instrument({
        name: 'KLM Case X01 - Travel Client',
        maxAge: 50
      })
      : []
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initInternationalisation,
      deps: [I18NService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: APIBaseInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
