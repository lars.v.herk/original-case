export const environment = {
  production: true,
  I18N_DEFAULT: 'nl',
  API_BASE: '/travel'
};
